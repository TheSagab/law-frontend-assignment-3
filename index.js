const express = require('express');
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');
const app = express();
const path = require('path');
const FormData = require('form-data');
const router = express.Router();
const port = process.env.port || 20105
const fetch = require("node-fetch");
const crypto = require("crypto");

app.use(bodyParser.json());         // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
})); 
app.use(fileUpload({
    limits: { fileSize: 50 * 1024 * 1024 },
}));

router.get('/',function(req, res){
    res.sendFile(path.join(__dirname+'/index.html'));
});

router.post('/upload',function(req, resp){
    const formData = new FormData();
    const routingKey = crypto.randomBytes(20).toString('hex');
    const bufferedFile = Buffer.from(JSON.stringify(req.files.file));
    formData.append('file', bufferedFile, req.files.file.name);
    const options = {
        method: "POST",
        headers: {
            "X-ROUTING-KEY": routingKey
        },
        body: formData
    };

    fetch('http://localhost:20106/', options).then(res => {
        res.json().then(json => {
            if (json.status) {
                resp.set('X-ROUTING-KEY', routingKey)
                console.log("ok")
                resp.send(renderResult(routingKey));
            }
            else {
                resp.send(json);
            }
        }).catch(err => resp.send(err));
    });
});

function renderResult(routingKey) {
    return `<!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>LAW Assignment 3</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
        <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
      </head>
      <body>
        <section class="section">
            <div class="container">
                <h1 class="title">
                    Zip Progress
                </h1>
                <div id="progress"></div>
            </div>
        </section>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.1.4/sockjs.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>
            <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
            <script type="text/javascript">
                WebSocketTest();
    
                function WebSocketTest() {
    
                    if ("WebSocket" in window) {
                        var ws_stomp_display = new SockJS('http://152.118.148.95:15674/stomp');
                        var client_display = Stomp.over(ws_stomp_display);
                        var mq_queue_display = "/exchange/1606879230/${routingKey}";
                        var on_connect_display = function() {
                            console.log('connected');
                            client_display.subscribe(mq_queue_display, on_message_display);
                        };
                        var on_error_display = function() {
                            console.log('error');
                        };
                        var on_message_display = function(m) {
                            console.log('message received');
                            $('#progress').html(m.body);
                        };
                        client_display.connect('0806444524', '0806444524', on_connect_display, on_error_display, '/0806444524');
                    } else {
                        // The browser doesn't support WebSocket
                        alert("WebSocket NOT supported by your Browser!");
                    }
                }
            </script>
      </body>
    </html>`
}

app.use('/', router);
app.listen(port);

console.log(`Example app listening on port ${port}!`);
